module goirate

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/gobuffalo/envy v1.10.1 // indirect
	github.com/gobuffalo/packd v1.0.1 // indirect
	github.com/gobuffalo/packr v1.30.1
	github.com/imerkle/go-qbittorrent v0.0.0-20200821182508-3ebb6c884f9d
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf
	github.com/jessevdk/go-flags v1.4.0
	github.com/olekukonko/tablewriter v0.0.2-0.20190618033246-cc27d85e17ce
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	gitlab.com/haath/gobytes v1.1.1
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
